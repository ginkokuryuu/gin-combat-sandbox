using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Enums
{
    public enum Weapons
    {
        GreatSword,
        Axe,
        OneHandedSword
    }

    public enum SceneEnum
    {
        Boot = 0,
        Level = 1,
        Reset = 2,
        Menu = 3
    }
}
