using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderReporter : MonoBehaviour
{
    private IColliderListener _parent;
    
    private void Awake()
    {
        _parent = transform.parent.GetComponent<IColliderListener>();
    }

    private void OnCollisionEnter(Collision other)
    {
        _parent.OnChildCollisionEnter(other);
    }

    private void OnCollisionExit(Collision other)
    {
        _parent.OnChildCollisionExit(other);
    }

    private void OnTriggerEnter(Collider other)
    {
        _parent.OnChildTriggerEnter(other);
    }

    private void OnTriggerExit(Collider other)
    {
        _parent.OnChildTriggerExit(other);
    }
}
