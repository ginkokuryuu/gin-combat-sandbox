using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IColliderListener
{
    public void OnChildCollisionEnter(Collision other);
    public void OnChildCollisionExit(Collision other);
    public void OnChildTriggerEnter(Collider other);
    public void OnChildTriggerExit(Collider other);
}
