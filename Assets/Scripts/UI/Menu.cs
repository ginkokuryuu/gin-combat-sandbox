using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    [HideInInspector]
    public UIStack CurrentStack;

    protected Canvas _canvas;

    protected virtual void Awake()
    {
        _canvas = GetComponentInChildren<Canvas>();
    }

    public virtual void OnPush()
    {
        
    }

    public virtual void OnPop()
    {
        
    }

    public virtual void Kill()
    {
        CurrentStack.DoPopMenu(this);
    }
}
