using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonSFX : MonoBehaviour
{
    [SerializeField] private ClipContainer _clickSFX;

    private void Awake()
    {
        if (SystemRoot.TryGetSystem(out CameraRigSystem cameraRigSystem))
        {
            AudioSource cameraAudioSource = cameraRigSystem.GetComponent<AudioSource>();
            GetComponent<Button>().onClick.AddListener(() =>
            {
                cameraAudioSource.PlayOneShot(_clickSFX.AudioClip, _clickSFX.Volume);
                Debug.Log(("Yeah"));
            });
        }
    }
}
