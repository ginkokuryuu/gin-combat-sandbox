using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class GameOverMenu : Menu
{
    [SerializeField] private PlayerDataSO _playerDataSo;

    [SerializeField] private TMP_Text _enemyKilledCount;
    [SerializeField] private Button _restartButton;
    [SerializeField] private Button _backMenuButton;

    public override void OnPush()
    {
        base.OnPush();

        _enemyKilledCount.text = _playerDataSo.EnemyKilled.ToString();
        
        _restartButton.onClick.AddListener(Restart);
        _backMenuButton.onClick.AddListener(BackToMenu);
    }

    public override void OnPop()
    {
        base.OnPop();
        Time.timeScale = 1f;
    }

    public void Restart()
    {
        if (SystemRoot.TryGetSystem(out LevelLoader levelLoader))
        {
            levelLoader.LoadLevel();
            Kill();
        }
    }

    public void BackToMenu()
    {
        if (SystemRoot.TryGetSystem(out LevelLoader levelLoader))
        {
            levelLoader.LoadReset();
            Kill();
        }
    }
}
