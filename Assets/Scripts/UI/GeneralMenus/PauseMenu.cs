using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenu : Menu
{
    [SerializeField] private Button _resumeButton;
    [SerializeField] private Button _backMenuButton;

    [Header("Game Events")]
    [SerializeField] private GameEvent _gamePausedEvent;
    [SerializeField] private GameEvent _gameResumedEvent;
    
    public override void OnPush()
    {
        base.OnPush();
        
        _resumeButton.onClick.AddListener(Resume);
        _backMenuButton.onClick.AddListener(BackToMenu);
        
        Time.timeScale = 0f;
        _gamePausedEvent.TriggerEvent();
    }

    public override void OnPop()
    {
        base.OnPop();
        Time.timeScale = 1f;
        _gameResumedEvent.TriggerEvent();
    }

    public void Resume()
    {
        if (SystemRoot.TryGetSystem(out LevelLoader levelLoader))
        {
            Kill();
        }
    }

    public void BackToMenu()
    {
        if (SystemRoot.TryGetSystem(out LevelLoader levelLoader))
        {
            levelLoader.LoadReset();
            Kill();
        }
    }
}
