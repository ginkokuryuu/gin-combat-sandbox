using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : Menu
{
    [SerializeField] private Button _startButton;
    [SerializeField] private Button _exitButton;

    public override void OnPush()
    {
        base.OnPush();
        Cursor.lockState = CursorLockMode.None;
        _startButton.onClick.AddListener(StartGame);
        _exitButton.onClick.AddListener(ExitGame);
    }

    public void StartGame()
    {
        if (SystemRoot.TryGetSystem(out LevelLoader levelLoader))
        {
            levelLoader.LoadLevel();
            Kill();
        }
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
