using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class InventoryMenu : Menu
{
    [SerializeField] private List<WeaponSO> _allWeapons;
    [SerializeField] private List<SkillSO> _allSkills;
    [SerializeField] private Button _closeButton;
    [SerializeField] private Button _selectButtonPrefab;
    
    [Header("Skill Selection")]
    [SerializeField] private Button _skillSelectButton;
    [SerializeField] private Button _closeSelectSkillButton;
    [SerializeField] private GameObject _skillSelectContainer;
    [SerializeField] private Transform _skillSelectLayout;

    [Header("Weapon Selection")]
    [SerializeField] private Button _weaponSelectButton;
    [SerializeField] private Button _closeSelectWeaponButton;
    [SerializeField] private GameObject _weaponSelectContainer;
    [SerializeField] private Transform _weaponSelectLayout;
    
    [Header("Game Events")]
    [SerializeField] private GameEvent _gamePausedEvent;
    [SerializeField] private GameEvent _gameResumedEvent;

    private InventorySystem _inventorySystem;

    public override void OnPush()
    {
        SystemRoot.TryGetSystem(out _inventorySystem);
        UpdateWeaponButton();
        UpdateSkillButton();
        
        GenerateWeaponButtons();
        GenerateSkillButtons();
        
        _weaponSelectButton.onClick.AddListener(OpenWeaponSelectUI);
        _closeSelectWeaponButton.onClick.AddListener(CloseWeaponSelectUI);
        
        _skillSelectButton.onClick.AddListener(OpenSkillSelectUI);
        _closeSelectSkillButton.onClick.AddListener(CloseSkillSelectUI);
        
        
        _closeButton.onClick.AddListener(CloseInventoy);

        Time.timeScale = 0f;
        _gamePausedEvent.TriggerEvent();
    }

    public override void OnPop()
    {
        Time.timeScale = 1f;
        _gameResumedEvent.TriggerEvent();
    }

    private void CloseInventoy()
    {
        Kill();
    }

    private void UpdateWeaponButton()
    {
        _weaponSelectButton.GetComponentInChildren<TMP_Text>().text = SystemRoot.Instance.Player.CurrentWeapon.WeaponType.ToString();
    }
    
    private void UpdateSkillButton()
    {
        _skillSelectButton.GetComponentInChildren<TMP_Text>().text = SystemRoot.Instance.Player.CurrentSkill.name;
    }

    private void GenerateWeaponButtons()
    {
        foreach (var weaponSo in _allWeapons)
        {
            Button button = Instantiate(_selectButtonPrefab, _weaponSelectLayout.transform);
            button.GetComponentInChildren<TMP_Text>().text = weaponSo.WeaponType.ToString();
            button.onClick.AddListener(() =>
            {
                WeaponSO selectedWeapon = weaponSo;
                _inventorySystem.ChangeWeapon(selectedWeapon);
                UpdateWeaponButton();
                CloseWeaponSelectUI();
            });
        }
    }
    
    private void GenerateSkillButtons(){
        foreach (var skill in _allSkills)
        {
            Button button = Instantiate(_selectButtonPrefab, _skillSelectLayout.transform);
            button.GetComponentInChildren<TMP_Text>().text = skill.name;
            button.onClick.AddListener(() =>
            {
                SkillSO selectedSkill = skill;
                _inventorySystem.ChangeSkill(selectedSkill);
                UpdateSkillButton();
                CloseSkillSelectUI();
            });
        }
    }

    private void OpenWeaponSelectUI()
    {
        _skillSelectContainer.SetActive(false);
        _weaponSelectContainer.SetActive(true);
    }
    
    private void CloseWeaponSelectUI()
    {
        _weaponSelectContainer.SetActive(false);
    }
    
    private void OpenSkillSelectUI()
    {
        _weaponSelectContainer.SetActive(false);
        _skillSelectContainer.SetActive(true);
    }
    
    private void CloseSkillSelectUI()
    {
        _skillSelectContainer.SetActive(false);
    }
}
