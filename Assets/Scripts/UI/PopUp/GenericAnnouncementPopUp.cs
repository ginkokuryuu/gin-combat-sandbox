using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GenericAnnouncementPopUp : Menu
{
    [SerializeField] private TMP_Text _popUpText;
    private float _aliveTime;

    public void Announce(string message, float time = 2f)
    {
        StopAllCoroutines();
        
        _popUpText.text = message;
        _aliveTime = time;
        gameObject.SetActive(true);
        
        StartCoroutine(PopUpCoroutine());
    }

    private IEnumerator PopUpCoroutine()
    {
        yield return new WaitForSeconds(_aliveTime);
        Kill();
    }

    public override void Kill()
    {
        gameObject.SetActive(false);
    }
}
