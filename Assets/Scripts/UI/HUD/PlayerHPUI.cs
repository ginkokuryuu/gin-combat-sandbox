using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlayerHPUI : MonoBehaviour
{
    [SerializeField] private PlayerDataSO _playerDataSO;
    [SerializeField] private TMP_Text _playerHPText;

    // Update is called once per frame
    void Update()
    {
        UpdateText();
    }

    private void UpdateText()
    {
        _playerHPText.text = $"{_playerDataSO.CurrentHealth}/{_playerDataSO.MaxHealth}";
    }
}
