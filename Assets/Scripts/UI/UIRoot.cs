using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIRoot : MonoBehaviour
{
    public static UIRoot Instance;
    
    [Header("UI Stacks")]
    [SerializeField] private UIStack _menuStack;
    [SerializeField] private UIStack _hudStack;
    [SerializeField] private UIStack _popUpStack;
    
    [Header("General Menu")]
    [SerializeField] private Menu _pauseMenu;
    [SerializeField] private Menu _playerHUD;
    [SerializeField] private GenericAnnouncementPopUp _genericAnnouncement;
    [SerializeField] private Menu _gameOverMenu;

    public GenericAnnouncementPopUp GenericAnnouncement => _genericAnnouncement;
    
    protected void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    public void OpenMenu(Menu menu)
    {
        _menuStack.DoPush(menu);
    }

    public void OpenHUD(Menu menu)
    {
        _hudStack.DoPush(menu);
    }

    public void OpenPauseMenu()
    {
        OpenMenu(_pauseMenu);
    }

    public void OpenPlayerHUD()
    {
        OpenHUD(_playerHUD);
    }

    public void OpenGameOverMenu()
    {
        OpenMenu(_gameOverMenu);
    }
}
