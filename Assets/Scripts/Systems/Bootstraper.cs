using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bootstraper : MonoBehaviour
{
#if UNITY_EDITOR
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void Init()
    {
        var currentlyLoadedEditorScene = SceneManager.GetActiveScene();
        
        if(currentlyLoadedEditorScene.name == "Boot")
        {
            return;
        }

        if (SceneManager.GetSceneByName("Boot").isLoaded != true)
        {
            SceneManager.LoadScene("Boot");
        }

        if (currentlyLoadedEditorScene.IsValid())
        {
            SceneManager.LoadSceneAsync(currentlyLoadedEditorScene.name, LoadSceneMode.Additive);
        }
    }
#endif
}
