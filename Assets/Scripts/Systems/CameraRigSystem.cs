using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class CameraRigSystem : MySystem
{
    [SerializeField] private float _smoothTime = 0.1f;
    [SerializeField] private Transform _followCameraPosition;

    private bool _isFollowing = false;
    
    private Vector3 _initCamPosition;
    private Quaternion _initCamRotation;

    private Vector2 _cameraTurn = new Vector2();
    private Vector3 _velocityRef;
    private Transform _followTarget;

    private Camera _camera;
    private Player _player;
    [SerializeField] private float _minManualPitch = 10.0f;
    [SerializeField] private float _maxManualPitch = 80f;


    private void Start()
    {
        _player = SystemRoot.Instance.Player;
        
        AttachCameraToRig();
        FollowPlayer();
        _player.RegisterListenerCameraMove(OnCameraMove);
    }
    
    private void Update()
    {
        if (_isFollowing)
        {
            MoveFollow();
        }
    }

    private void OnDestroy()
    {
        _player.UnregisterListenerCameraMove(OnCameraMove);
    }

    private void OnCameraMove(Vector2 deltaPos)
    {
        float prevPitch = _cameraTurn.y;

        _cameraTurn.y -= deltaPos.y;
        _cameraTurn.x += deltaPos.x;
        
        Quaternion updatedRotation = Quaternion.Euler(_cameraTurn.y, _cameraTurn.x, 0f);
        Vector3 euler = updatedRotation.eulerAngles;
        float currentPitch = euler.x > 180f ? euler.x - 360f : euler.x;
        bool beyondPitch = (currentPitch < _minManualPitch - 0.0001f) || (currentPitch > _maxManualPitch + 0.0001f);
        
        if (beyondPitch)
        {
            transform.rotation = Quaternion.Euler(prevPitch, _cameraTurn.x, 0f);
            _cameraTurn.y = prevPitch;
        }
        else
        {
            transform.rotation = updatedRotation;
        }
    }

    private void MoveFollow()
    {
        transform.position = Vector3.SmoothDamp(transform.position, _followTarget.position, ref _velocityRef, _smoothTime);
    }

    public void AttachCameraToRig()
    {
        _camera = Camera.main;
        
        if (_camera != null)
        {
            _initCamPosition = _camera.transform.position;
            _initCamRotation = _camera.transform.rotation;
            _camera.transform.parent = _followCameraPosition;
            _camera.transform.SetLocalPositionAndRotation(Vector3.zero, Quaternion.identity);
        }
    }

    public void FollowPlayer()
    {
        _followTarget = _player.transform;
        _isFollowing = true;
    }
}
