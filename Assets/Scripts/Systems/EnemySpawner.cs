using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Pool;
using Random = UnityEngine.Random;

public class EnemySpawner : MySystem
{
    [Header("Game Events")]
    [SerializeField] private GameEvent _gameEndEvent;
    [SerializeField] private GameEvent _enemyKilledEvent;

    [Header("Enemy Prefabs")]
    [SerializeField] private Enemy _meleeEnemyPrefab;
    [SerializeField] private List<Enemy> _allEnemyPrefabs = new List<Enemy>();

    [Header("Spawn Parameter")]
    [SerializeField] private float _initEnemySpawnCount = 2f;
    [SerializeField] private float _spawnCooldown = 10f;
    [SerializeField] private float _spawnRateIncreaseOverTime = 0.5f;
    [SerializeField] private float _waveToSpawnRange = 2;
    
    [Header("Wave Parameter")]
    [SerializeField] private int _waveSpawnCount = 3;
    [SerializeField] private float _waveCooldown = 10f;

    private int _selectedIndex;
    private List<ObjectPool<Enemy>> _enemyPools = new List<ObjectPool<Enemy>>();
    private ObjectPool<Enemy> _enemyPool;
    
    private List<Transform> _spawnPositions = new List<Transform>();
    private bool _isSpawningEnemy = false;
    
    private float _enemySpawnCount = 2f;
    private int _spawnCounter = 0;
    private int _waveCounter = 0;
    private int _spawnedEnemyCount = 0;

    private Coroutine _spawnLoopCoroutine;
    private AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        _enemyKilledEvent.AddListener(OnEnemyKilledEvent);
        
        InitializeEnemyPools();
    }

    private void InitializeEnemyPools()
    {
        for (int i = 0; i < _allEnemyPrefabs.Count; i++)
        {
            ObjectPool<Enemy> enemyPool = new ObjectPool<Enemy>(OnPOCreate, OnPOGet, OnPORelease, OnPODestryed, defaultCapacity: 20);
            _enemyPools.Add(enemyPool);
        }
    }

    private void ClearEnemyPools()
    {
        foreach (var enemyPool in _enemyPools)
        {
            enemyPool.Clear();
        }
    }

    #region Object Pool
    private void OnPODestryed(Enemy enemy)
    {   
        if(enemy != null)
            Destroy(enemy.gameObject);
    }

    private void OnPORelease(Enemy enemy)
    {
        enemy.gameObject.SetActive(false);
    }

    private void OnPOGet(Enemy enemy)
    {
        
    }

    private Enemy OnPOCreate()
    {
        Enemy spawnedEnemy = Instantiate(_allEnemyPrefabs[_selectedIndex]);
        return spawnedEnemy;
    }
    #endregion 

    private void OnDestroy()
    {
        _enemyKilledEvent.RemoveListener(OnEnemyKilledEvent);
    }

    private void OnEnemyKilledEvent()
    {
        _spawnedEnemyCount -= 1;
        if (_spawnedEnemyCount <= 0 && _spawnCounter >= _waveSpawnCount)
        {
            StartCoroutine(WaveCooldown());
        }
    }

    public void InitArena(List<Transform> spawnPositions)
    {
        StopAllCoroutines();
        ClearEnemyPools();
        
        _audioSource.Play();
        _spawnPositions = spawnPositions;
        _isSpawningEnemy = true;
        _enemySpawnCount = _initEnemySpawnCount;
        _spawnCounter = 0;
        _waveCounter = 0;
        _spawnedEnemyCount = 0;
        
        _gameEndEvent.AddListener(OnGameEndEvent);
        StartSpawnLoop();
    }

    private void StartSpawnLoop()
    {
        _spawnLoopCoroutine = StartCoroutine(SpawnLoop());
    }

    private IEnumerator SpawnLoop()
    {
        UIRoot.Instance.GenericAnnouncement.Announce($"WAVE {_waveCounter + 1} START!");
        while (_isSpawningEnemy)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(_spawnCooldown);
        }
    }

    private IEnumerator WaveCooldown()
    {
        UIRoot.Instance.GenericAnnouncement.Announce($"WAVE {_waveCounter + 1} DONE!");
        _waveCounter += 1;
        
        yield return new WaitForSeconds(_waveCooldown);
        
        _spawnCounter = 0;
        _isSpawningEnemy = true;
        
        StartSpawnLoop();
    }

    private void SpawnEnemy()
    {
        var random = new System.Random();
        var selectedPos = _spawnPositions.OrderBy((item) => random.Next())
            .Take(Mathf.FloorToInt(_enemySpawnCount)).ToList();

        int indexLimit = _waveCounter >= _waveToSpawnRange ? _allEnemyPrefabs.Count - 1 : 0;
        foreach(var position in selectedPos)
        {
            _selectedIndex = Math.Clamp(Random.Range(0, _allEnemyPrefabs.Count), 0, indexLimit);
            ObjectPool<Enemy> selectedPool = _enemyPools[_selectedIndex];
            Enemy spawnedEnemy = selectedPool.Get();
            spawnedEnemy.SetPoolObject(selectedPool);
            
            spawnedEnemy.gameObject.SetActive(true);
            spawnedEnemy.ResetData(position.position);
            
            _spawnedEnemyCount += 1;
        }

        _enemySpawnCount += _spawnRateIncreaseOverTime;
        _spawnCounter += 1;

        if (_spawnCounter >= _waveSpawnCount)
        {
            _isSpawningEnemy = false;
            StopCoroutine(_spawnLoopCoroutine);
        }
    }

    private void OnGameEndEvent()
    {
        _isSpawningEnemy = false;
        _gameEndEvent.RemoveListener(OnGameEndEvent);
        StopAllCoroutines();
    }
}
