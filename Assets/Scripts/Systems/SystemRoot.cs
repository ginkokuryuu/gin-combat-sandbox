using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SystemRoot : MonoBehaviour
{
    public static SystemRoot Instance;
    
    public Player Player { private set; get; }
    
    private Dictionary<Type, MySystem> _systems = new Dictionary<Type, MySystem>();
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        
        MySystem[] systems = transform.GetComponentsInChildren<MySystem>();
        foreach (var system in systems)
        {
            _systems.Add(system.GetType(), system);
        }

        Player = transform.GetComponentInChildren<Player>();
    }

    private void Start()
    {
        if (TryGetSystem(out LevelLoader levelLoader))
        {
            levelLoader.LoadMenu();
        }
    }

    private bool SystemExist<T>() where T : MySystem
    {
        return _systems.ContainsKey(typeof(T));
    }

    public static bool TryGetSystem<T>(out T system) where T : MySystem
    {
        if (Instance != null && Instance.SystemExist<T>())
        {
            system = Instance.GetSystem<T>();
            return true;
        }
        system = null;
        return false;
    }

    private T GetSystem<T>() where T : MySystem
    {
        return _systems[typeof(T)] as T;
    }
}
