using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader : MySystem
{
    [Header("Game Events")]
    [SerializeField] private GameEvent _levelLoadedEvent;
    
    public void Start()
    {
        
    }

    public void LoadLevel()
    {
        StartCoroutine(LoadSceneAsync((int)Enums.SceneEnum.Level, () => _levelLoadedEvent.TriggerEvent()));
    }

    public void LoadMenu()
    {
        StartCoroutine(LoadSceneAsync((int)Enums.SceneEnum.Menu));
    }
    
    public void LoadReset()
    {
        StartCoroutine(LoadSceneAsync((int)Enums.SceneEnum.Reset));
    }

    private IEnumerator LoadSceneAsync(int sceneIndex, Action callback = null)
    {
        AsyncOperation loadScene = SceneManager.LoadSceneAsync(sceneIndex);
        while (!loadScene.isDone)
        {
            yield return 0;
        }
        callback?.Invoke();
    }
}
