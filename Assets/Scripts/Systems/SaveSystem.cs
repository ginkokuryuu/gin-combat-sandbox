using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSystem : MySystem
{
    [SerializeField] private PlayerDataSO _playerDataSO;
    
    public void LoadData()
    {
        _playerDataSO.AxeUnlocked = PlayerPrefs.GetInt(SaveDataEnums.AxeUnlocked, 0) == 1;
    }

    public void SaveData()
    {
        PlayerPrefs.SetInt(SaveDataEnums.AxeUnlocked, _playerDataSO.AxeUnlocked ? 1 : 0);
    }
}

public static class SaveDataEnums
{
    public const string AxeUnlocked = "AxeUnlocked";
}
