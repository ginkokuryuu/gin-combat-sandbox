using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private int _dodgeLayer;

    [Header("Audio")]
    [SerializeField] private ClipContainer _footstepSFX;
    private AudioSource _audioSource;

    private int _defaultLayer;
    private bool _isDodging = false;
    
    private bool _isDisabled = false;
    private Vector2 _inputVec = new Vector2();
    private Vector3 _directionVector = new Vector3();

    private CharacterController _characterController;
    private Animator _animator;
    private Transform _camera;
    
    private static readonly int IsMoving = Animator.StringToHash("IsMoving");
    public Vector3 Direction { get; private set; }

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
        _defaultLayer = gameObject.layer;
    }

    private void Start()
    {
        _camera = Camera.main.transform;
    }

    private void Update()
    {
        MovePlayer();
    }

    public void ResetData()
    {
        _isDodging = false;
        _isDisabled = false;
        _inputVec = new Vector2();
        _directionVector = new Vector3();
    }

    public void OnMovement(InputAction.CallbackContext context)
    {
        _inputVec = context.ReadValue<Vector2>();
        _directionVector = NormalizeInputToCamera(_inputVec);

        _animator.SetBool(IsMoving, _directionVector != Vector3.zero);
    }

    public void OnDodge(InputAction.CallbackContext context)
    {
        if (context.started && !_isDodging)
        {
            Dodge();
        }
    }

    private void Dodge()
    {
        RearrangePlayerRotation(true);
        _animator.Play("Dodge", 0, 0);
    }

    private void MovePlayer()
    {
        if(_isDodging) return;
        _directionVector = NormalizeInputToCamera(_inputVec);
        RearrangePlayerRotation();
    }
    
    private Vector3 NormalizeInputToCamera(Vector2 inputVector)
    {
        Vector3 vecForward = _camera.forward;
        Vector3 vecRight = _camera.right;
        vecForward.y = 0;
        vecRight.y = 0;
        vecForward.Normalize();
        vecRight.Normalize();
    
        return vecForward * inputVector.y + vecRight * inputVector.x;
    }

    public void DisableMovement()
    {
        _isDisabled = true;
    }

    public void EnableMovement()
    {
        _isDisabled = false;
        RearrangePlayerRotation();
    }

    public void EnterRoll()
    {
        _isDodging = true;
        ToggleLayer(true);
    }

    public void ExitRoll()
    {
        _isDodging = false;
        ToggleLayer(false);
    }

    public void ToggleLayer(bool isInvincible)
    {
        gameObject.layer = isInvincible ? _dodgeLayer : _defaultLayer;
    }

    public void PlayFootstep()
    {
        _audioSource.PlayOneShot(_footstepSFX.AudioClip, _footstepSFX.Volume);
    }

    private void OnAnimatorMove()
    {
        Vector3 velocity = _animator.deltaPosition;
        _characterController.Move(velocity);
    }

    private void RearrangePlayerRotation(bool force = false)
    {
        if (_directionVector != Vector3.zero)
        {
            transform.forward = Direction = (_isDisabled && !force) ? transform.forward : _directionVector;
        }
    }
}
