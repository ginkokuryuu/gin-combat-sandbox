using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    [SerializeField] private PlayerDataSO _playerDataSo;
    
    [Header("Default Data")] 
    [SerializeField] private WeaponSO _defaultWeapon;
    [SerializeField] private SkillSO _defaultSkill;

    [Header("Avatar Reference")] 
    [SerializeField] private Transform _rightHandPivot;

    [Header("Game Events")]
    [SerializeField] private GameEvent _levelLoadedEvent;
    [SerializeField] private GameEvent _gameEndEvent;
    [SerializeField] private GameEvent _gamePausedEvent;
    [SerializeField] private GameEvent _gameResumedEvent;
    [SerializeField] private GameEvent _enemyKilledEvent;

    [Header("Audio")]
    [SerializeField] private AudioClip _attackedSFX;
    [SerializeField] private AudioClip _attackedSFX2;
    private AudioSource _audioSource;
    
    private PlayerCombat _playerCombat;
    private PlayerMovement _playerMovement;
    private PlayerInput _playerInput;
    private event Action<Vector2> _cameraMoveAction;

    private SaveSystem _saveSystem;

    public WeaponSO CurrentWeapon => _playerCombat.CurrentWeaponData;
    public SkillSO CurrentSkill => _playerCombat.CurrentSkillData;

    private void Awake()
    {
        _playerInput = GetComponent<PlayerInput>();
        _playerCombat = GetComponent<PlayerCombat>();
        _playerMovement = GetComponent<PlayerMovement>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _levelLoadedEvent.AddListener(OnLevelLoadedEvent);
        _gamePausedEvent.AddListener(OnGamePausedEvent);
        _gameResumedEvent.AddListener(OnGameResumedEvent);
        _enemyKilledEvent.AddListener(OnEnemyKilledEvent);

        if (SystemRoot.TryGetSystem(out _saveSystem))
        {
            _saveSystem.LoadData();
            _playerDataSo.RefreshData();
        }
    }


    private void OnDestroy()
    {
        _levelLoadedEvent.RemoveListener(OnLevelLoadedEvent);
        _gamePausedEvent.RemoveListener(OnGamePausedEvent);
        _gameResumedEvent.RemoveListener(OnGameResumedEvent);
        _enemyKilledEvent.RemoveListener(OnEnemyKilledEvent);
    }

    private void OnEnemyKilledEvent()
    {
        _playerDataSo.EnemyKilled += 1;
    }
    
    private void OnLevelLoadedEvent()
    {
        gameObject.SetActive(true);
        EnablePlayerInput();
        
        _playerDataSo.RefreshData();
        _playerCombat.EquipWeapon(CurrentWeapon ? CurrentWeapon : _defaultWeapon, _rightHandPivot);
        _playerCombat.ChangeSkill(CurrentSkill ? CurrentSkill : _defaultSkill);
        _playerMovement.ResetData();

        Cursor.lockState = CursorLockMode.Locked;
        UIRoot.Instance.OpenPlayerHUD();
    }

    private void OnGamePausedEvent()
    {
        Cursor.lockState = CursorLockMode.None;
        DisablePlayerInput();
    }

    private void OnGameResumedEvent()
    {
        Cursor.lockState = CursorLockMode.Locked;
        EnablePlayerInput();
    }

    private void Death()
    {
        gameObject.SetActive(false);
        _gameEndEvent.TriggerEvent();
        Cursor.lockState = CursorLockMode.None;
        UIRoot.Instance.OpenGameOverMenu();
    }

    public void ChangeWeapon(WeaponSO weaponSO)
    {
        _playerCombat.EquipWeapon(weaponSO, _rightHandPivot);
    }

    public void ChangeSkill(SkillSO skillSO)
    {
        _playerCombat.ChangeSkill(skillSO);
    }

    public void RegisterListenerCameraMove(Action<Vector2> listener)
    {
        _cameraMoveAction += listener;
    }
    
    public void UnregisterListenerCameraMove(Action<Vector2> listener)
    {
        _cameraMoveAction -= listener;
    }

    public void OnCameraMove(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            _cameraMoveAction?.Invoke(context.ReadValue<Vector2>());
        }
    }

    public void EnablePlayerInput()
    {
        _playerInput.enabled = true;
    }

    public void DisablePlayerInput()
    {
        _playerInput.enabled = false;
    }

    public void Attacked(int damage)
    {
        _playerDataSo.CurrentHealth -= damage;
        Debug.Log($"{gameObject.name} damaged, current health: {_playerDataSo.CurrentHealth}");
        if (_playerDataSo.CurrentHealth <= 0)
        {
            Death();
            return;
        }
        _playerCombat.Attacked();
    }

    public void OnPause(InputAction.CallbackContext context)
    {
        if(context.performed)
            UIRoot.Instance.OpenPauseMenu();
    }

    public void Heal(int healingAmount)
    {
        _playerDataSo.CurrentHealth =
            Math.Clamp(_playerDataSo.CurrentHealth + healingAmount, 0, _playerDataSo.MaxHealth);
    }
}
