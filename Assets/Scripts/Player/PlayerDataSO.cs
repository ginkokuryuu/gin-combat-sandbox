using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Data/Create Player Data", fileName = "Player Data")]
public class PlayerDataSO : ScriptableObject
{
    public int MaxHealth;
    
    //Runtime Value
    [HideInInspector] public int CurrentHealth;
    [HideInInspector] public bool AxeUnlocked;
    [HideInInspector] public WeaponSO EquippedWeapon;
    [HideInInspector] public int EnemyKilled;

    public void RefreshData()
    {
        CurrentHealth = MaxHealth;
        EnemyKilled = 0;
    }
}
