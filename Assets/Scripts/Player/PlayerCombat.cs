using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Serialization;

public class PlayerCombat : MonoBehaviour
{
    [SerializeField] private Transform _rightHandPivot;
    
    [Header("Audio")]
    [SerializeField] private ClipContainer _attackSFX;
    [SerializeField] private ClipContainer _attackedSFX;
    [SerializeField] private ClipContainer _attackedGroanSFX;
    private AudioSource _audioSource;
    
    private WeaponSO _currentWeaponData;
    private Weapon _currentWeapon;
    private float _leftClickCooldown;
    private float _lastLeftClickTime;
    private float _comboResetCounterTime;
    private int _comboCounter;

    private SkillSO _currentSkillData;
    private float _rightClickCooldown;
    private float _lastRightClickTime;
    private RuntimeAnimatorController _previousRuntimeAC;
    
    private bool _isFlinched = false;

    private Animator _animator;
    private PlayerMovement _playerMovement;
    
    private static readonly int AttackSpeed = Animator.StringToHash("AttackSpeed");

    public WeaponSO CurrentWeaponData => _currentWeaponData;
    public SkillSO CurrentSkillData => _currentSkillData;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _playerMovement = GetComponent<PlayerMovement>();
        _audioSource = GetComponent<AudioSource>();
    }

    private void Attack()
    {
        if(_isFlinched) return;
        
        float lastClickDiff = Time.time - _lastLeftClickTime;
        if (lastClickDiff < _leftClickCooldown) return;
        if (lastClickDiff >= _comboResetCounterTime)
        {
            _comboCounter = 0;
        }

        AttackSO attackData = _currentWeaponData.AttackCombo[_comboCounter];
        _animator.runtimeAnimatorController = attackData.AnimatorOverrideController;
        _leftClickCooldown = attackData.MoveCooldown;
        _comboResetCounterTime = _currentWeaponData.ComboResetTime + _leftClickCooldown;
        _animator.SetFloat(AttackSpeed, attackData.SpeedModifier);
        
        _animator.Play("Attack", 0, attackData.StartOverride);
        _comboCounter += 1;
        _lastLeftClickTime = Time.time;
        _currentWeapon.SetDamage(attackData.AttackDamage);

        if (_comboCounter >= _currentWeaponData.AttackCombo.Count)
        {
            _comboCounter = 0;
        }
    }

    private void CastSkill()
    {
        if(_isFlinched) return;

        float lastClickDiff = Time.time - _lastRightClickTime;
        if(lastClickDiff < _rightClickCooldown) return;
        
        _previousRuntimeAC = _animator.runtimeAnimatorController;
        _animator.runtimeAnimatorController = _currentSkillData.AnimatorOverrideController;
        _rightClickCooldown = _currentSkillData.MoveCooldown;
        
        _animator.Play("Skill", 0, 0);
        _lastRightClickTime = Time.time;
    }

    public void Attacked()
    {
        _animator.Play("Flinch", 0, 0);
    }

    public void EquipWeapon(WeaponSO weaponSo, Transform handPivot)
    {
        if(handPivot.childCount > 0)
            Destroy(handPivot.GetChild(0).gameObject);
        
        _currentWeaponData = weaponSo;
        _currentWeapon = Instantiate(weaponSo.WeaponPrefab, handPivot.position, handPivot.rotation, handPivot);

        _animator.runtimeAnimatorController = _currentWeaponData.AnimatorOverrideController;
    }
    
    public void ChangeSkill(SkillSO skillSO)
    {
        _currentSkillData = skillSO;
    }

    public void OnMouseClick(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            Attack();
        }
    }

    public void OnSkillInput(InputAction.CallbackContext context)
    {
        if (context.started)
        {
            CastSkill();
        }
    }

    public void CastSkillEvent()
    {
        _currentSkillData.CastSkill(_rightHandPivot, _playerMovement.Direction, SystemRoot.Instance.Player);
        _audioSource.PlayOneShot(_currentSkillData.CastSFX.AudioClip, _currentSkillData.CastSFX.Volume);
    }

    public void EnterAttack()
    {
        _playerMovement.DisableMovement();
        _currentWeapon.EnableCollider();
        
        _audioSource.PlayOneShot(_attackSFX.AudioClip, _attackSFX.Volume);
    }

    public void ExitAttack()
    {
        _playerMovement.EnableMovement();
        _currentWeapon.DisableCollider();
    }

    public void EnterFlinch()
    {
        _playerMovement.DisableMovement();
        _playerMovement.ToggleLayer(true);
        _isFlinched = true;
        
        _audioSource.PlayOneShot(_attackedSFX.AudioClip, _attackedSFX.Volume);
        _audioSource.PlayOneShot(_attackedGroanSFX.AudioClip, _attackedGroanSFX.Volume);
    }

    public void ExitFlinch()
    {
        _playerMovement.EnableMovement();
        _playerMovement.ToggleLayer(false);
        _isFlinched = false;
    }

    public void EnterSkill()
    {
        _playerMovement.DisableMovement();
    }

    public void ExitSkill()
    {
        _playerMovement.EnableMovement();
        _animator.runtimeAnimatorController = _previousRuntimeAC;
    }
}
