using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackState : StateMachineBehaviour
{
    private PlayerCombat _playerCombat;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _playerCombat = _playerCombat ? _playerCombat : animator.GetComponent<PlayerCombat>();
        _playerCombat.EnterAttack();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _playerCombat = _playerCombat ? _playerCombat : animator.GetComponent<PlayerCombat>();
        _playerCombat.ExitAttack();
    }
}
