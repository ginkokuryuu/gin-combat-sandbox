using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour, IColliderListener
{
    private int _weaponDamage;
    private Collider _weaponCollider;

    private List<Enemy> _attackedEnemies = new List<Enemy>();
    
    // Start is called before the first frame update
    void Start()
    {
        _weaponCollider = GetComponentInChildren<Collider>();
        _weaponCollider.enabled = false;
    }

    private void AttackEnemy(Enemy enemy)
    {
        if(_attackedEnemies.Contains(enemy)) return;
        
        enemy.Attacked(_weaponDamage);
        _attackedEnemies.Add(enemy);
    }

    public void EnableCollider()
    {
        _weaponCollider.enabled = true;
    }

    public void DisableCollider()
    {
        _weaponCollider.enabled = false;
        _attackedEnemies.Clear();
    }
    
    public void SetDamage(int damage)
    {
        _weaponDamage = damage;
    }

    #region IColliderListener Implementation
    public void OnChildCollisionEnter(Collision other)
    {
        
    }

    public void OnChildCollisionExit(Collision other)
    {
        
    }

    public void OnChildTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Enemy enemy))
        {
            AttackEnemy(enemy);
        }
    }

    public void OnChildTriggerExit(Collider other)
    {
        
    }
    #endregion
}
