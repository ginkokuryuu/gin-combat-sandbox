using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Combat/Create New Weapon", fileName = "New Weapon SO")]
public class WeaponSO : ScriptableObject
{
    [Header("Weapon Data")] 
    public Enums.Weapons WeaponType;
    public Weapon WeaponPrefab;
    
    [Header("Attack Data")]
    public List<AttackSO> AttackCombo = new List<AttackSO>();
    public float ComboResetTime = 1f;

    [Header("Animation Override")] 
    public AnimatorOverrideController AnimatorOverrideController;
}
