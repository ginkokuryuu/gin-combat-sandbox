using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRollState : StateMachineBehaviour
{
    private PlayerMovement _playerMovement;
    
    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _playerMovement = _playerMovement ? _playerMovement : animator.GetComponent<PlayerMovement>();
        _playerMovement.EnterRoll();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _playerMovement = _playerMovement ? _playerMovement : animator.GetComponent<PlayerMovement>();
        _playerMovement.ExitRoll();
    }
}
