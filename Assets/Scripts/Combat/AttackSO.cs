using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Combat/Create Attack SO", fileName = "New Attack SO")]
public class AttackSO : ScriptableObject
{
    public AnimatorOverrideController AnimatorOverrideController;
    public float MoveCooldown;
    public float StartOverride;
    public float SpeedModifier = 1f;
    public int AttackDamage;
}
