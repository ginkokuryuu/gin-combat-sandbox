using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Combat/Skill/Create General Skill", fileName = "New General Skill")]
public class SkillSO : ScriptableObject
{
    public AnimatorOverrideController AnimatorOverrideController;
    public float MoveCooldown;
    public ClipContainer CastSFX;
    public ParticleSystem CastVFX;

    public virtual void CastSkill(Transform spawnPosition, Vector3 direction, Player player)
    {
        return;
    }
}