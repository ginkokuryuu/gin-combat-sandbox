using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Combat/Skill/Create Fireball Skill", fileName = "Fireball Skill")]
public class FireballSO : SkillSO
{
    public Fireball FireballPrefab;
    public float FireballSpeed;
    public int FireballDamage;
    public float FireballLifetime;
    
    public override void CastSkill(Transform spawnPosition, Vector3 direction, Player player)
    {
        Fireball spawnedFireball = Instantiate(FireballPrefab, spawnPosition.position, Quaternion.identity);
        spawnedFireball.Launch(direction, FireballSpeed, FireballLifetime, FireballDamage);
    }
}
