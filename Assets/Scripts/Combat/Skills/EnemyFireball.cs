using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFireball : Fireball
{
    protected override void DamageTarget()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, _explosionRadius);

        foreach (var other in colliders)
        {
            if (other.TryGetComponent(out Player player))
            {
                player.Attacked(_damage);
            }
        }
    }

    protected override void TryToExplode(Collider other)
    {
        if (!other.TryGetComponent(out Enemy enemy))
        {
            Explode();
        }
    }
}
