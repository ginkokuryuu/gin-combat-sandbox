using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fireball : MonoBehaviour
{
    [SerializeField] protected float _explosionRadius = 2f;
    [SerializeField] private ClipContainer _explodeSFX;
    [SerializeField] private ParticleSystem _explodeVFX;
    protected int _damage;
    private Rigidbody _rb;
    
    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }

    public void Launch(Vector3 direction, float speed, float lifeTime, int fireballDamage)
    {
        _rb.velocity = direction * speed;
        _damage = fireballDamage;
        Destroy(gameObject, lifeTime);
    }

    private void OnTriggerEnter(Collider other)
    {
        TryToExplode(other);
    }

    protected void Explode()
    {
        DamageTarget();
        AudioSource.PlayClipAtPoint(_explodeSFX.AudioClip, transform.position, _explodeSFX.Volume);
        ParticleSystem explodeVFX = Instantiate(_explodeVFX, transform.position, Quaternion.identity);
        explodeVFX.transform.localScale = Vector3.one * _explosionRadius;
        Destroy(explodeVFX.gameObject, _explodeVFX.main.duration);
        Destroy(gameObject);
    }

    protected virtual void DamageTarget()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, _explosionRadius);

        foreach (var other in colliders)
        {
            if (other.TryGetComponent(out Enemy enemy))
            {
                enemy.Attacked(_damage);
            }
        }
    }

    protected virtual void TryToExplode(Collider other)
    {
        if (!other.TryGetComponent(out Player player))
        {
            Explode();
        }
    }
}
