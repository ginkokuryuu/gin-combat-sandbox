using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SO/Combat/Skill/Create Healing Skill", fileName = "Healing Skill")]
public class HealingSO : SkillSO
{
    public int HealingAmount;
    
    public override void CastSkill(Transform spawnPosition, Vector3 direction, Player player)
    {
        player.Heal(HealingAmount);
        
        Vector3 position = player.transform.position;
        position.y = 0.25f;
        ParticleSystem vfx = Instantiate(CastVFX, position, Quaternion.Euler(-90f, 0, 0));
        Destroy(vfx.gameObject, vfx.main.duration);
    }
}
