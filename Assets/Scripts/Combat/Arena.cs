using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arena : MonoBehaviour
{
    [SerializeField] private Transform _playerSpawnPosition;
    [SerializeField] private List<Transform> _spawnPositions = new List<Transform>();
    
    // Start is called before the first frame update
    void Start()
    {
        if (SystemRoot.TryGetSystem(out EnemySpawner spawner))
        {
            spawner.InitArena(_spawnPositions);
            SystemRoot.Instance.Player.transform.position = _playerSpawnPosition.position;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
