using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkillState : StateMachineBehaviour
{
    private PlayerCombat _playerCombat;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _playerCombat = _playerCombat ? _playerCombat : animator.GetComponent<PlayerCombat>();
        _playerCombat.EnterSkill();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _playerCombat = _playerCombat ? _playerCombat : animator.GetComponent<PlayerCombat>();
        _playerCombat.ExitSkill();
    }
}
