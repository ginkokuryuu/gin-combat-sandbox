using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class RangeEnemy : Enemy
{
    [Header("Range Enemy Specific")]
    [SerializeField] private Transform spawnPosition;
    [SerializeField] private Fireball _fireballPrefab;
    [SerializeField] private float _fireballSpeed;
    [SerializeField] private float _fireballLifetime;

    public void CastAttack()
    {
        transform.forward = _playerTransform.position - transform.position;
        Vector3 direction = transform.forward;
        _audioSource.PlayOneShot(_attackSFX.AudioClip, _attackSFX.Volume);
        
        Fireball spawnedFireball = Instantiate(_fireballPrefab, spawnPosition.position, Quaternion.identity);
        spawnedFireball.Launch(direction, _fireballSpeed, _fireballLifetime, _attackDamage);
    }
}
