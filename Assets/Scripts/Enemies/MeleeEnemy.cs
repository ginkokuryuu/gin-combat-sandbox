using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeEnemy : Enemy
{
    [Header("Melee Enemy Specific")]
    [SerializeField] private EnemyAttackCollider _attackCollider;

    protected override void Start()
    {
        base.Start();
        _attackCollider.SetDamage(_attackDamage);
    }

    public override void ResetData(Vector3 startPosition)
    {
        base.ResetData(startPosition);
        _attackCollider.DisableCollider();
    }

    public override void EnterAttack()
    {
        base.EnterAttack();
        _audioSource.PlayOneShot(_attackSFX.AudioClip, _attackSFX.Volume);
        _attackCollider.EnableCollider();
    }

    public override void ExitAttack()
    {
        base.ExitAttack();
        _attackCollider.DisableCollider();
    }
}
