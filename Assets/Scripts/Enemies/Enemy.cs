using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Pool;

public class Enemy : MonoBehaviour
{
    [Header("Enemy Data")]
    [SerializeField] protected int _health;
    [SerializeField] protected float _attackDistance = 1f;
    [SerializeField] protected float _attackCooldown = 1f;
    [SerializeField] protected int _attackDamage = 1;


    [Header("Game Events")]
    [SerializeField] private GameEvent _gameEndEvent;
    [SerializeField] private GameEvent _enemyKilledEvent;

    [Header("Audio")]
    [SerializeField] private ClipContainer _footstepSFX;
    [SerializeField] protected ClipContainer _attackSFX;
    [SerializeField] private ClipContainer _attackedSFX;
    protected AudioSource _audioSource;

    [Header("VFX")]
    [SerializeField] private ParticleSystem _deathVFX;

    protected int _currentHealth;
    protected float _lastAttackTime = 0f;
    protected bool _isMoving = false;
    
    protected Animator _animator;
    protected CharacterController _characterController;

    protected Transform _playerTransform;
    protected NavMeshAgent _navMeshAgent;

    private ObjectPool<Enemy> _objectPool;
    
    private static readonly int IsMoving = Animator.StringToHash("IsMoving");

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _animator = GetComponent<Animator>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    protected virtual void Start()
    {
        _playerTransform = SystemRoot.Instance.Player.transform;
        _gameEndEvent.AddListener(OnGameEndEvent);
    }

    public virtual void ResetData(Vector3 startPosition)
    {
        StopAllCoroutines();
        _currentHealth = _health;
        ToggleMovement(false);
        _navMeshAgent.Warp(startPosition);

        StartCoroutine(WaitThenMove());
    }
    
    private IEnumerator WaitThenMove()
    {
        yield return new WaitForSeconds(1f);
        ToggleMovement(true);
    }

    // Update is called once per frame
    void Update()
    {
        _navMeshAgent.SetDestination(_playerTransform.position);
        if(!_isMoving) return;
        
        float distance = Vector3.Distance(transform.position, _playerTransform.position);
        if (distance <= _attackDistance)
        {
            TryToAttack();
        }
    }

    private void TryToAttack()
    {
        float lastAttackDiff = Time.time - _lastAttackTime;
        if (lastAttackDiff < _attackCooldown)
            return;

        ToggleMovement(false);
        _animator.Play("Attack", 0, 0);
    }
    
    private void OnAnimatorMove()
    {
        if (_isMoving)
        {
            Vector3 velocity = _animator.deltaPosition;
            _characterController.Move(velocity);
        }
    }

    private void OnGameEndEvent()
    {
        if (gameObject.activeSelf)
        {
            gameObject.SetActive(false);
            _objectPool.Release(this);
        }
    }

    private void OnDestroy()
    {
        _gameEndEvent.RemoveListener(OnGameEndEvent);
    }

    private void Death()
    {
        _navMeshAgent.isStopped = true;
        _isMoving = false;
        _animator.SetBool(IsMoving, false);

        ParticleSystem particle = Instantiate(_deathVFX, transform.position, Quaternion.Euler(90, 0, 0));
        Destroy(particle.gameObject, particle.main.duration);
        
        gameObject.SetActive(false);
        _objectPool.Release(this);
        _enemyKilledEvent.TriggerEvent();
    }

    private IEnumerator WaitBeforeActive()
    {
        yield return new WaitForSeconds(_attackCooldown);
        ToggleMovement(true);
    }
    
    protected void ToggleMovement(bool value)
    {
        Debug.Log(value);
        _isMoving = value;
        _navMeshAgent.isStopped = !_isMoving;
        _animator.SetBool(IsMoving, _isMoving);
    }

    public void SetPoolObject(ObjectPool<Enemy> objectPool)
    {
        _objectPool = objectPool;
    }

    public void Attacked(int damage)
    {
        _currentHealth -= damage;
        if (_currentHealth <= 0)
        {
            AudioSource.PlayClipAtPoint(_attackedSFX.AudioClip, transform.position, _attackedSFX.Volume);
            Death();
        }
        else
        {
            _audioSource.PlayOneShot(_attackedSFX.AudioClip, _attackedSFX.Volume);
            _animator.Play("Flinch", 0, 0);
        }
    }

    public virtual void EnterAttack()
    {
        ToggleMovement(false);
    }

    public virtual void ExitAttack()
    {
        _lastAttackTime = Time.time;
        StartCoroutine(WaitBeforeActive());
    }

    public void EnterFlinch()
    {
        ToggleMovement(false);
    }

    public void ExitFlinch()
    {
        ToggleMovement(true);
    }

    public void PlayFootstep()
    {
        _audioSource.PlayOneShot(_footstepSFX.AudioClip, _footstepSFX.Volume);
    }
}
