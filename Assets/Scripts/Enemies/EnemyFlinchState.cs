using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlinchState : StateMachineBehaviour
{
    private Enemy _enemy;

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _enemy = _enemy ? _enemy : animator.GetComponent<Enemy>();
        _enemy.EnterFlinch();
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _enemy = _enemy ? _enemy : animator.GetComponent<Enemy>();
        _enemy.ExitFlinch();
    }
}
