using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ClipContainer
{
    public AudioClip AudioClip;
    public float Volume = 1f;
}
