using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InventorySystem : MySystem
{
    [SerializeField] private Menu _inventoryMenu;
    
    [Header("For Testing Only")]
    [SerializeField] private List<WeaponSO> _tempWeapons = new List<WeaponSO>();
    private int _weaponCounter = 0;
    [SerializeField] private List<SkillSO> _tempSkills = new List<SkillSO>();
    private int _skillCounter = 0;

    private Player _player;
    
    private void Start()
    {
        _player = SystemRoot.Instance.Player;
    }

    private void Update()
    {
        // if (Input.GetKeyDown(KeyCode.P))
        // {
        //     _weaponCounter += 1;
        //     if (_weaponCounter >= _tempWeapons.Count)
        //         _weaponCounter = 0;
        //     _player.ChangeWeapon(_tempWeapons[_weaponCounter]);
        // }
        // if (Input.GetKeyDown(KeyCode.O))
        // {
        //     _skillCounter += 1;
        //     if (_skillCounter >= _tempSkills.Count)
        //         _skillCounter = 0;
        //     _player.ChangeSkill(_tempSkills[_skillCounter]);
        // }
    }
    
    public void OnInventory(InputAction.CallbackContext context)
    {
        if(context.performed)
            UIRoot.Instance.OpenMenu(_inventoryMenu);
    }

    public void ChangeWeapon(WeaponSO weapon)
    {
        _player.ChangeWeapon(weapon);
    }

    public void ChangeSkill(SkillSO selectedSkill)
    {
        _player.ChangeSkill(selectedSkill);
    }
}
